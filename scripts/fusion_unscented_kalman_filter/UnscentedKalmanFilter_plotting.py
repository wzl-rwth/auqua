import numpy as np
import pylab as pl
from pykalman import UnscentedKalmanFilter
import csv

# -- load data
data1 = np.load('Imputed_1_w51.npy', allow_pickle=True)
#data2 = np.load('Imputed_2_w51.npy', allow_pickle=True)
data3 = np.load('Imputed_3_w51.npy', allow_pickle=True)
data4 = np.load('Imputed_4_w51.npy', allow_pickle=True)
data5 = np.load('Imputed_5_w51.npy', allow_pickle=True)

# Empty Array for result
init = np.empty((np.shape(data1)[0],7))
# initial state
#initial_state_mean = 0

# - Filter set up
observation_covariance = np.array([[1000, 0, 0, 0],
                                   [0, 1000, 0, 0],
                                   [0, 0, 1000, 0],
                                   [0, 0, 0, 1000]])
# -- define array to append data
pos_data = np.array([[0, 0, 0, 0]])
for k in range(7):
    # -- loop through data

    for i in range(data1.shape[0]):
        # - measurements for each time step
        measurements = np.array([[data1[i, 0, k], data3[i, 0, k], data4[i, 0, k], data5[i, 0, k]]])

        # - append measurements
        pos_data = np.append(pos_data, measurements, axis=0)

    # - delete initial array
    pos_data_new = np.delete(pos_data, 0, 0)
    pos_data = np.array([[0,0,0,0]])
    initial_state_mean = np.nanmean(pos_data_new)
    # - Mask invalid array data
    masked_array = np.ma.masked_invalid(pos_data_new)

    # Split Array in column
    col = np.hsplit(pos_data_new, 4)
    col_1 = col[0]
    col_3 = col[1]
    col_4 = col[2]
    col_5 = col[3]

    # - Apply UnscentedKalmanFilter
    UKF = UnscentedKalmanFilter(n_dim_state=1, n_dim_obs=4,initial_state_mean=initial_state_mean, observation_covariance=observation_covariance)

    # - State Estimation
    filtered_state_estimates = UKF.filter(Z=masked_array)[0]
    filtered_state_estimates = np.squeeze(filtered_state_estimates)

    # - put states in Array
    inbs = np.arange(0,data1.shape[0])
    np.put(init, (inbs*7+k), filtered_state_estimates)

    # - Plot estimates
    pl.figure()
    lines_1 = pl.plot(col_1, color='b', ls=':')
    lines_3 = pl.plot(col_3, color='g', ls=':')
    lines_4 = pl.plot(col_4, color='c', ls=':')
    lines_5 = pl.plot(col_5, color='m', ls=':')

    lines_filt = pl.plot(filtered_state_estimates, color='r', ls='--')

    pl.legend((lines_1[0], lines_3[0], lines_4[0], lines_5[0], lines_filt[0]),
              ('Cam1', 'Cam2', 'Cam3', 'Cam4', 'UKF'), loc='lower right')

    #pl.title('Montagedaten ID 4 (Kessel)')
    pl.ylabel('Z-Achse')
    pl.xlabel('Frame')
    pl.show()


# save result
np.save('Estimated_States', init)
init = init.tolist()
with open('Estimated.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile, delimiter=',')
    writer.writerows(init)
