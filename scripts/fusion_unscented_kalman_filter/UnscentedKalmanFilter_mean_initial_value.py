import numpy as np
import pylab as pl
from pykalman import UnscentedKalmanFilter
import csv

# -- load data
data1 = np.load('Imputed_1_w51.npy', allow_pickle=True)
#data2 = np.load('Imputed_2_w51.npy', allow_pickle=True)
data3 = np.load('Imputed_3_w51.npy', allow_pickle=True)
data4 = np.load('Imputed_4_w51.npy', allow_pickle=True)
data5 = np.load('Imputed_5_w51.npy', allow_pickle=True)
"""
data1 = np.load('Preprocessed_1.npy', allow_pickle=True)
#data2 = np.load('Preprocessed_2.npy', allow_pickle=True)
data3 = np.load('Preprocessed_3.npy', allow_pickle=True)
data4 = np.load('Preprocessed_4.npy', allow_pickle=True)
data5 = np.load('Preprocessed_5.npy', allow_pickle=True)
"""
# Empty Array for result
init = np.empty((np.shape(data1)[0], 7))

"""
-------------------------!!!only possible with initial data values which are not Nonen, else set fix initial value!!!
"""

# - Filter set up
observation_covariance = np.array([[5000, 0, 0, 0],
                                   [0, 5000, 0, 0],
                                   [0, 0, 5000, 0],
                                   [0, 0, 0, 5000]])

# -- define array to append data
pos_data = np.array([[0, 0, 0, 0]])
for k in range(7):
    # -- loop through data

    for i in range(data1.shape[0]):
        # - measurements for each time step
        measurements = np.array([[data1[i, 0, k], data3[i, 0, k], data4[i, 0, k], data5[i, 0, k]]])

        # - append measurements
        pos_data = np.append(pos_data, measurements, axis=0)

    # - delete initial array
    pos_data_new = np.delete(pos_data, 0, 0)
    pos_data = np.array([[0,0,0,0]])

    # - Mask invalid array data
    masked_array = np.ma.masked_invalid(pos_data_new)

    # initial state mean
    initial_state_mean = np.nanmean(pos_data_new[0])

    # - Apply UnscentedKalmanFilter
    UKF = UnscentedKalmanFilter(n_dim_state=1, n_dim_obs=4,initial_state_mean=initial_state_mean,
                                observation_covariance=observation_covariance)

    # - State Estimation
    filtered_state_estimates = UKF.filter(Z=masked_array)[0]
    filtered_state_estimates = np.squeeze(filtered_state_estimates)

    # - put states in Array
    inbs = np.arange(0,data1.shape[0])
    np.put(init, (inbs * 7 + k), filtered_state_estimates)

res0 = init
init = np.empty((np.shape(data1)[0], 7))
#---------------------------------------------------------------------------------------------------
for k in range(7):
    # -- loop through data

    for i in range(data1.shape[0]):
        # - measurements for each time step
        measurements = np.array([[data1[i, 1, k], data3[i, 1, k], data4[i, 1, k], data5[i, 1, k]]])

        # - append measurements
        pos_data = np.append(pos_data, measurements, axis=0)

    # - delete initial array
    pos_data_new = np.delete(pos_data, 0, 0)
    pos_data = np.array([[0,0,0,0]])

    # - Mask invalid array data
    masked_array = np.ma.masked_invalid(pos_data_new)

    # initial state
    initial_state_mean = np.nanmean(pos_data_new[0])

    # - Apply UnscentedKalmanFilter
    UKF = UnscentedKalmanFilter(n_dim_state=1, n_dim_obs=4,initial_state_mean=initial_state_mean,
                                observation_covariance=observation_covariance)

    # - State Estimation
    filtered_state_estimates = UKF.filter(Z=masked_array)[0]
    filtered_state_estimates = np.squeeze(filtered_state_estimates)

    # - put states in Array
    inbs = np.arange(0,data1.shape[0])
    np.put(init, (inbs * 7 + k), filtered_state_estimates)

res1 = init
init = np.empty((np.shape(data1)[0], 7))
#---------------------------------------------------------------------------------------------------
for k in range(7):
    # -- loop through data

    for i in range(data1.shape[0]):
        # - measurements for each time step
        measurements = np.array([[data1[i, 2, k], data3[i, 2, k], data4[i, 2, k], data5[i, 2, k]]])

        # - append measurements
        pos_data = np.append(pos_data, measurements, axis=0)

    # - delete initial array
    pos_data_new = np.delete(pos_data, 0, 0)
    pos_data = np.array([[0,0,0,0]])

    # - Mask invalid array data
    masked_array = np.ma.masked_invalid(pos_data_new)

    # initial state
    initial_state_mean = np.nanmean(pos_data_new[0])

    # - Apply UnscentedKalmanFilter
    UKF = UnscentedKalmanFilter(n_dim_state=1, n_dim_obs=4,initial_state_mean=initial_state_mean,
                                observation_covariance=observation_covariance)

    # - State Estimation
    filtered_state_estimates = UKF.filter(Z=masked_array)[0]
    filtered_state_estimates = np.squeeze(filtered_state_estimates)

    # - put states in Array
    inbs = np.arange(0,data1.shape[0])
    np.put(init, (inbs * 7 + k), filtered_state_estimates)

res2 = init
init = np.empty((np.shape(data1)[0], 7))
#---------------------------------------------------------------------------------------------------
for k in range(7):
    # -- loop through data

    for i in range(data1.shape[0]):
        # - measurements for each time step
        measurements = np.array([[data1[i, 3, k], data3[i, 3, k], data4[i, 3, k], data5[i, 3, k]]])

        # - append measurements
        pos_data = np.append(pos_data, measurements, axis=0)

    # - delete initial array
    pos_data_new = np.delete(pos_data, 0, 0)
    pos_data = np.array([[0,0,0,0]])

    # - Mask invalid array data
    masked_array = np.ma.masked_invalid(pos_data_new)

    # initial state
    initial_state_mean = np.nanmean(pos_data_new[0])

    # - Apply UnscentedKalmanFilter
    UKF = UnscentedKalmanFilter(n_dim_state=1, n_dim_obs=4,initial_state_mean=initial_state_mean,
                                observation_covariance=observation_covariance)

    # - State Estimation
    filtered_state_estimates = UKF.filter(Z=masked_array)[0]
    filtered_state_estimates = np.squeeze(filtered_state_estimates)

    # - put states in Array
    inbs = np.arange(0,data1.shape[0])
    np.put(init, (inbs * 7 + k), filtered_state_estimates)

res3 = init
init = np.empty((np.shape(data1)[0], 7))
#---------------------------------------------------------------------------------------------------
for k in range(7):
    # -- loop through data

    for i in range(data1.shape[0]):
        # - measurements for each time step
        measurements = np.array([[data1[i, 4, k], data3[i, 4, k], data4[i, 4, k], data5[i, 4, k]]])

        # - append measurements
        pos_data = np.append(pos_data, measurements, axis=0)

    # - delete initial array
    pos_data_new = np.delete(pos_data, 0, 0)
    pos_data = np.array([[0,0,0,0]])

    # - Mask invalid array data
    masked_array = np.ma.masked_invalid(pos_data_new)

    # initial state
    initial_state_mean = np.nanmean(pos_data_new[0])

    # - Apply UnscentedKalmanFilter
    UKF = UnscentedKalmanFilter(n_dim_state=1, n_dim_obs=4,initial_state_mean=initial_state_mean,
                                observation_covariance=observation_covariance)

    # - State Estimation
    filtered_state_estimates = UKF.filter(Z=masked_array)[0]
    filtered_state_estimates = np.squeeze(filtered_state_estimates)

    # - put states in Array
    inbs = np.arange(0,data1.shape[0])
    np.put(init, (inbs * 7 + k), filtered_state_estimates)

res4 = init
init = np.empty((np.shape(data1)[0], 7))
#---------------------------------------------------------------------------------------------------
for k in range(7):
    # -- loop through data

    for i in range(data1.shape[0]):
        # - measurements for each time step
        measurements = np.array([[data1[i, 5, k], data3[i, 5, k], data4[i, 5, k], data5[i, 5, k]]])

        # - append measurements
        pos_data = np.append(pos_data, measurements, axis=0)

    # - delete initial array
    pos_data_new = np.delete(pos_data, 0, 0)
    pos_data = np.array([[0,0,0,0]])

    # - Mask invalid array data
    masked_array = np.ma.masked_invalid(pos_data_new)

    # initial state
    initial_state_mean = np.nanmean(pos_data_new[0])

    # - Apply UnscentedKalmanFilter
    UKF = UnscentedKalmanFilter(n_dim_state=1, n_dim_obs=4,initial_state_mean=initial_state_mean,
                                observation_covariance=observation_covariance)

    # - State Estimation
    filtered_state_estimates = UKF.filter(Z=masked_array)[0]
    filtered_state_estimates = np.squeeze(filtered_state_estimates)

    # - put states in Array
    inbs = np.arange(0,data1.shape[0])
    np.put(init, (inbs * 7 + k), filtered_state_estimates)

res5 = init
init = np.empty((np.shape(data1)[0], 7))
#---------------------------------------------------------------------------------------------------
for k in range(7):
    # -- loop through data

    for i in range(data1.shape[0]):
        # - measurements for each time step
        measurements = np.array([[data1[i, 1, k], data3[i, 6, k], data4[i, 6, k], data5[i, 6, k]]])

        # - append measurements
        pos_data = np.append(pos_data, measurements, axis=0)

    # - delete initial array
    pos_data_new = np.delete(pos_data, 0, 0)
    pos_data = np.array([[0,0,0,0]])

    # - Mask invalid array data
    masked_array = np.ma.masked_invalid(pos_data_new)

    # initial state
    initial_state_mean = np.nanmean(pos_data_new[0])

    # - Apply UnscentedKalmanFilter
    UKF = UnscentedKalmanFilter(n_dim_state=1, n_dim_obs=4,initial_state_mean=initial_state_mean,
                                observation_covariance=observation_covariance)

    # - State Estimation
    filtered_state_estimates = UKF.filter(Z=masked_array)[0]
    filtered_state_estimates = np.squeeze(filtered_state_estimates)

    # - put states in Array
    inbs = np.arange(0,data1.shape[0])
    np.put(init, (inbs * 7 + k), filtered_state_estimates)

res6 = init
init = np.empty((np.shape(data1)[0], 7))
#---------------------------------------------------------------------------------------------------
for k in range(7):
    # -- loop through data

    for i in range(data1.shape[0]):
        # - measurements for each time step
        measurements = np.array([[data1[i, 7, k], data3[i, 7, k], data4[i, 7, k], data5[i, 7, k]]])

        # - append measurements
        pos_data = np.append(pos_data, measurements, axis=0)

    # - delete initial array
    pos_data_new = np.delete(pos_data, 0, 0)
    pos_data = np.array([[0,0,0,0]])

    # - Mask invalid array data
    masked_array = np.ma.masked_invalid(pos_data_new)

    # initial state
    initial_state_mean = np.nanmean(pos_data_new[0])

    # - Apply UnscentedKalmanFilter
    UKF = UnscentedKalmanFilter(n_dim_state=1, n_dim_obs=4,initial_state_mean=initial_state_mean,
                                observation_covariance=observation_covariance)

    # - State Estimation
    filtered_state_estimates = UKF.filter(Z=masked_array)[0]
    filtered_state_estimates = np.squeeze(filtered_state_estimates)

    # - put states in Array
    inbs = np.arange(0,data1.shape[0])
    np.put(init, (inbs * 7 + k), filtered_state_estimates)

res7 = init
init = np.empty((np.shape(data1)[0], 7))
#---------------------------------------------------------------------------------------------------
for k in range(7):
    # -- loop through data

    for i in range(data1.shape[0]):
        # - measurements for each time step
        measurements = np.array([[data1[i, 8, k], data3[i, 8, k], data4[i, 8, k], data5[i, 8, k]]])

        # - append measurements
        pos_data = np.append(pos_data, measurements, axis=0)

    # - delete initial array
    pos_data_new = np.delete(pos_data, 0, 0)
    pos_data = np.array([[0,0,0,0]])

    # - Mask invalid array data
    masked_array = np.ma.masked_invalid(pos_data_new)

    # initial state
    initial_state_mean = np.nanmean(pos_data_new[0])

    # - Apply UnscentedKalmanFilter
    UKF = UnscentedKalmanFilter(n_dim_state=1, n_dim_obs=4,initial_state_mean=initial_state_mean,
                                observation_covariance=observation_covariance)

    # - State Estimation
    filtered_state_estimates = UKF.filter(Z=masked_array)[0]
    filtered_state_estimates = np.squeeze(filtered_state_estimates)

    # - put states in Array
    inbs = np.arange(0,data1.shape[0])
    np.put(init, (inbs * 7 + k), filtered_state_estimates)

res8 = init
init = np.empty((np.shape(data1)[0], 7))
#---------------------------------------------------------------------------------------------------
for k in range(7):
    # -- loop through data

    for i in range(data1.shape[0]):
        # - measurements for each time step
        measurements = np.array([[data1[i, 9, k], data3[i, 9, k], data4[i, 9, k], data5[i, 9, k]]])

        # - append measurements
        pos_data = np.append(pos_data, measurements, axis=0)

    # - delete initial array
    pos_data_new = np.delete(pos_data, 0, 0)
    pos_data = np.array([[0,0,0,0]])

    # - Mask invalid array data
    masked_array = np.ma.masked_invalid(pos_data_new)

    # initial state
    initial_state_mean = np.nanmean(pos_data_new[0])

    # - Apply UnscentedKalmanFilter
    UKF = UnscentedKalmanFilter(n_dim_state=1, n_dim_obs=4,initial_state_mean=initial_state_mean,
                                observation_covariance=observation_covariance)

    # - State Estimation
    filtered_state_estimates = UKF.filter(Z=masked_array)[0]
    filtered_state_estimates = np.squeeze(filtered_state_estimates)

    # - put states in Array
    inbs = np.arange(0,data1.shape[0])
    np.put(init, (inbs * 7 + k), filtered_state_estimates)

res9 = init
init = np.empty((np.shape(data1)[0], 7))
#---------------------------------------------------------------------------------------------------

result = np.stack((res0, res1, res2, res3, res4, res5, res6, res7, res8, res9),axis=1)

np.save('M1_Estimated_States_imp_OC5000.npy', result)

result = result.tolist()
with open('M1_Estimated_States_imp_OC5000.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile, delimiter=',')
    writer.writerows(result)
