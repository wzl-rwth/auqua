import numpy as np
import impyute as imp
import csv

"""
imputation window, slices through array in defined window, None values are replaced with locf-algorithm
"""

# -- Settings -------------------------------------------------------
# set window length
win_len = 51
data = np.load('Preprocessed_5.npy', allow_pickle=True)
data = data.astype('float')

#--------------------------------------------------------------------
# Column 0 -----------------------------------------------------
# initial array to append
data_0 = np.zeros((1, 7), dtype=float)
for i in range(data.shape[0]):
    data_0 = np.append(data_0, np.array([data[i, 0]]), axis=0)
# delete first entry
data_0 = np.delete(data_0, 0, 0)
# initial indices
indices0 = np.arange(7*win_len)
# Maximum windows
x0 = len(data_0)/win_len
x0 = np.trunc(x0)
x0 = int(x0)

# Column 1 -----------------------------------------------------
data_1 = np.zeros((1, 7), dtype=float)
for i in range(data.shape[0]):
    data_1 = np.append(data_1, np.array([data[i, 1]]), axis=0)
data_1 = np.delete(data_1, 0, 0)
indices1 = np.arange(7*win_len)
x1 = len(data_1)/win_len
x1 = np.trunc(x1)
x1 = int(x1)

# Column 2 -----------------------------------------------------
data_2 = np.zeros((1, 7), dtype=float)
for i in range(data.shape[0]):
    data_2 = np.append(data_2, np.array([data[i, 2]]), axis=0)
data_2 = np.delete(data_2, 0, 0)
indices2 = np.arange(7*win_len)
x2 = len(data_2)/win_len
x2 = np.trunc(x2)
x2 = int(x2)

# Column 3 -----------------------------------------------------
data_3 = np.zeros((1, 7), dtype=float)
for i in range(data.shape[0]):
    data_3 = np.append(data_3, np.array([data[i, 3]]), axis=0)
data_3 = np.delete(data_3, 0, 0)
indices3 = np.arange(7*win_len)
x3 = len(data_3)/win_len
x3 = np.trunc(x3)
x3 = int(x3)

# Column 4 -----------------------------------------------------
data_4 = np.zeros((1, 7), dtype=float)
for i in range(data.shape[0]):
    data_4 = np.append(data_4, np.array([data[i, 4]]), axis=0)
data_4 = np.delete(data_4, 0, 0)
indices4 = np.arange(7*win_len)
x4 = len(data_4)/win_len
x4 = np.trunc(x4)
x4 = int(x4)

# Column 5 -----------------------------------------------------
data_5 = np.zeros((1, 7), dtype=float)
for i in range(data.shape[0]):
    data_5 = np.append(data_5, np.array([data[i, 5]]), axis=0)
data_5 = np.delete(data_5, 0, 0)
indices5 = np.arange(7*win_len)
x5 = len(data_5)/win_len
x5 = np.trunc(x5)
x5 = int(x5)

# Column 6 -----------------------------------------------------
data_6 = np.zeros((1, 7), dtype=float)
for i in range(data.shape[0]):
    data_6 = np.append(data_6, np.array([data[i, 6]]), axis=0)
data_6 = np.delete(data_6, 0, 0)
indices6 = np.arange(7*win_len)
x6 = len(data_6)/win_len
x6 = np.trunc(x6)
x6 = int(x6)

# Column 7 -----------------------------------------------------
data_7 = np.zeros((1, 7), dtype=float)
for i in range(data.shape[0]):
    data_7 = np.append(data_7, np.array([data[i, 7]]), axis=0)
data_7 = np.delete(data_7, 0, 0)
indices7 = np.arange(7*win_len)
x7 = len(data_7)/win_len
x7 = np.trunc(x7)
x7 = int(x7)

# Column 8 -----------------------------------------------------
data_8 = np.zeros((1, 7), dtype=float)
for i in range(data.shape[0]):
    data_8 = np.append(data_8, np.array([data[i, 8]]), axis=0)
data_8 = np.delete(data_8, 0, 0)
indices8 = np.arange(7*win_len)
x8 = len(data_8)/win_len
x8 = np.trunc(x8)
x8 = int(x8)

# Column 9 -----------------------------------------------------
data_9 = np.zeros((1, 7), dtype=float)
for i in range(data.shape[0]):
    data_9 = np.append(data_9, np.array([data[i, 9]]), axis=0)
data_9 = np.delete(data_9, 0, 0)
indices9 = np.arange(7*win_len)
x9 = len(data_9)/win_len
x9 = np.trunc(x9)
x9 = int(x9)
#--------------------------------------------------------------------

# Column 0 -----------------------------------------------------
p0 = 0
for k in range(x0):
    # define window
    window = data_0[p0:p0 + win_len]
    # check for NaN in Window
    check_Nans = np.isnan(window)
    if np.isnan(window[0,0]) == False and True in check_Nans:
        # locf in window
        window = imp.locf(window, axis=1)
        # return results
        p0 = p0 + win_len
        np.put(data_0, indices0, window)
        indices0 = indices0 + 7 * win_len
    else:
        p0 = p0 + win_len
        indices0 = indices0 + 7 * win_len

# Column 1 -----------------------------------------------------
p1 = 0
for k in range(x1):
    window = data_1[p1:p1 + win_len]
    check_Nans = np.isnan(window)
    if np.isnan(window[0,0]) == False and True in check_Nans:
        window = imp.locf(window, axis=1)
        np.put(data_1, indices1, window)
        p1 = p1 + win_len
        indices1 = indices1 + 7 * win_len
    else:
        p1 = p1 + win_len
        indices1 = indices1 + 7 * win_len

# Column 2 -----------------------------------------------------
p2 = 0
for k in range(x2):
    window = data_2[p2:p2 + win_len]
    check_Nans = np.isnan(window)
    if np.isnan(window[0,0]) == False and True in check_Nans:
        window = imp.locf(window, axis=1)
        np.put(data_2, indices2, window)
        p2 = p2 + win_len
        indices2 = indices2 + 7 * win_len
    else:
        p2 = p2 + win_len
        indices2 = indices2 + 7 * win_len

# Column 3 -----------------------------------------------------
p3 = 0
for k in range(x3):
    window = data_3[p3:p3 + win_len]
    check_Nans = np.isnan(window)
    if np.isnan(window[0,0]) == False and True in check_Nans:
        window = imp.locf(window, axis=1)
        np.put(data_3, indices3, window)
        p3 = p3 + win_len
        indices3 = indices3 + 7 * win_len
    else:
        p3 = p3 + win_len
        indices3 = indices3 + 7 * win_len

# Column 4 -----------------------------------------------------
p4 = 0
for k in range(x4):
    window = data_4[p4:p4 + win_len]
    check_Nans = np.isnan(window)
    if np.isnan(window[0,0]) == False and True in check_Nans:
        window = imp.locf(window, axis=1)
        np.put(data_4, indices4, window)
        p4 = p4 + win_len
        indices4 = indices4 + 7 * win_len
    else:
        p4 = p4 + win_len
        indices4 = indices4 + 7 * win_len

# Column 5 -----------------------------------------------------
p5 = 0
for k in range(x5):
    window = data_5[p5:p5 + win_len]
    check_Nans = np.isnan(window)
    if np.isnan(window[0,0]) == False and True in check_Nans:
        window = imp.locf(window, axis=1)
        np.put(data_5, indices5, window)
        p5 = p5 + win_len
        indices5 = indices5 + 7 * win_len
    else:
        p5 = p5 + win_len
        indices5 = indices5 + 7 * win_len

# Column 6 -----------------------------------------------------
p6 = 0
for k in range(x6):
    window = data_6[p6:p6 + win_len]
    check_Nans = np.isnan(window)
    if np.isnan(window[0,0]) == False and True in check_Nans:
        window = imp.locf(window, axis=1)
        np.put(data_6, indices6, window)
        p6 = p6 + win_len
        indices6 = indices6 + 7 * win_len
    else:
        p6 = p6 + win_len
        indices6 = indices6 + 7 * win_len

# Column 7 -----------------------------------------------------
p7 = 0
for k in range(x7):
    window = data_7[p7:p7 + win_len]
    check_Nans = np.isnan(window)
    if np.isnan(window[0,0]) == False and True in check_Nans:
        window = imp.locf(window, axis=1)
        np.put(data_7, indices7, window)
        p7 = p7 + win_len
        indices7 = indices7 + 7 * win_len
    else:
        p7 = p7 + win_len
        indices7 = indices7 + 7 * win_len

# Column 8 -----------------------------------------------------
p8 = 0
for k in range(x8):
    window = data_8[p8:p8 + win_len]
    check_Nans = np.isnan(window)
    if np.isnan(window[0,0]) == False and True in check_Nans:
        window = imp.locf(window, axis=1)
        np.put(data_8, indices8, window)
        p8 = p8 + win_len
        indices8 = indices8 + 7 * win_len
    else:
        p8 = p8 + win_len
        indices8 = indices8 + 7 * win_len

# Column 9 -----------------------------------------------------
p9 = 0
for k in range(x9):
    window = data_9[p9:p9 + win_len]
    check_Nans = np.isnan(window)
    if np.isnan(window[0,0]) == False and True in check_Nans:
        window = imp.locf(window, axis=1)
        np.put(data_9, indices9, window)
        p9 = p9 + win_len
        indices9 = indices9 + 7 * win_len
    else:
        p9 = p9 + win_len
        indices9 = indices9 + 7 * win_len

# Resultarray and saving --------------------------------------------
result = np.stack((data_0, data_1, data_2, data_3, data_4, data_5, data_6, data_7, data_8, data_9), axis=1)
np.save('Imputed_5_w51', result)
result = result.tolist()
with open('Imputed_5_w51.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile, delimiter=',')
    writer.writerows(result)