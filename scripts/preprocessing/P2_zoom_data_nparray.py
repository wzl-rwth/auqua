import numpy as np
from scipy.ndimage import zoom
import csv

"""
arrays from the cameras and of different are zoomed to the smallest array length
"""

# -- load NaNtoNum data
data1 = np.load('NaNtoNum_1.npy', allow_pickle=True)
#data2 = np.load('NaNtoNum_2.npy', allow_pickle=True)
data3 = np.load('NaNtoNum_3.npy', allow_pickle=True)
data4 = np.load('NaNtoNum_4.npy', allow_pickle=True)
data5 = np.load('NaNtoNum_5.npy', allow_pickle=True)

# -- get maximum shape (first dim)
minshape = min(np.shape(data1)[0], np.shape(data3)[0], np.shape(data4)[0], np.shape(data5)[0])

# -- calculate zoom rate (downsizing)
x_1 = minshape/np.shape(data1)[0]
#x_2 = minshape/np.shape(data2)[0]
x_3 = minshape/np.shape(data3)[0]
x_4 = minshape/np.shape(data4)[0]
x_5 = minshape/np.shape(data5)[0]

"""
nearest: last pixel replicating forward
constant better than nearest
"""

# -- Zoom Array's
zoomed_1 = zoom(data1, (x_1, 1, 1,), output=None, order=3, mode='constant', cval=0.0, prefilter=True, grid_mode=False)
#zoomed_2 = zoom(data2, (x_2, 1, 1,), output=None, order=3, mode='constant', cval=0.0, prefilter=True, grid_mode=False)
zoomed_3 = zoom(data3, (x_3, 1, 1,), output=None, order=3, mode='constant', cval=0.0, prefilter=True, grid_mode=False)
zoomed_4 = zoom(data4, (x_4, 1, 1,), output=None, order=3, mode='constant', cval=0.0, prefilter=True, grid_mode=False)
zoomed_5 = zoom(data5, (x_5, 1, 1,), output=None, order=3, mode='constant', cval=0.0, prefilter=True, grid_mode=False)

# -- save as npy and csv
np.save('ZoomedArray_1', zoomed_1)
zoomed_1 = zoomed_1.tolist()
with open('Zoomed_1.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile, delimiter=',')
    writer.writerows(zoomed_1)
#--------------------------------------------------------------
"""
np.save('ZoomedArray_2', zoomed_2)
zoomed_2 = zoomed_2.tolist()
with open('Zoomed_2.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile, delimiter=',')
    writer.writerows(zoomed_2)
"""
#--------------------------------------------------------------
np.save('ZoomedArray_3', zoomed_3)
zoomed_3 = zoomed_3.tolist()
with open('Zoomed_3.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile, delimiter=',')
    writer.writerows(zoomed_3)
#--------------------------------------------------------------

np.save('ZoomedArray_4', zoomed_4)
zoomed_4 = zoomed_4.tolist()
with open('Zoomed_4.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile, delimiter=',')
    writer.writerows(zoomed_4)

# --------------------------------------------------------------
np.save('ZoomedArray_5', zoomed_5)
zoomed_5 = zoomed_5.tolist()
with open('Zoomed_5.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile, delimiter=',')
    writer.writerows(zoomed_5)
