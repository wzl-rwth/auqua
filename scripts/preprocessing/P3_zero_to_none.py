import numpy as np
import csv

"""
set back impossible values to None
"""

# -- load zoomed Array's
Zoomed_1 = np.load('ZoomedArray_1.npy', allow_pickle=True)
#Zoomed_2 = np.load('ZoomedArray_2.npy', allow_pickle=True)
Zoomed_3 = np.load('ZoomedArray_3.npy', allow_pickle=True)
Zoomed_4 = np.load('ZoomedArray_4.npy', allow_pickle=True)
Zoomed_5 = np.load('ZoomedArray_5.npy', allow_pickle=True)

# -- Array for Comparison
comp = np.zeros(7)
# -- Array without fill value
fill = np.array([None, None, None, None, None, None, None])
# -- Nums for Iteration through Array
inbs1 = np.array([0, 1, 2, 3, 4, 5, 6])
#inbs2 = np.array([0, 1, 2, 3, 4, 5, 6])
inbs3 = np.array([0, 1, 2, 3, 4, 5, 6])
inbs4 = np.array([0, 1, 2, 3, 4, 5, 6])
inbs5 = np.array([0, 1, 2, 3, 4, 5, 6])
# -- Max Id's
max_ids = 10

# -- Filtering invalid values and replace
for k in range(np.shape(Zoomed_1)[0]):
    for i in range(np.shape(Zoomed_1)[1]):
        new = Zoomed_1[k, i]
        new = np.absolute(new)
        new = np.trunc(new)
        if (new == comp).all() == True:
            np.put(Zoomed_1, (inbs1 + (i * 7)), fill)
        else:
            continue
    inbs1 = inbs1 + 7 * max_ids
"""
# -- Filtering invalid values and replace
for k in range(np.shape(Zoomed_2)[0]):
    for i in range(np.shape(Zoomed_2)[1]):
        new = Zoomed_2[k, i]
        new = np.absolute(new)
        new = np.trunc(new)
        if ((new == comp).all()) == True:
            np.put(Zoomed_2, (inbs2 + (i * 7)), fill)
        else:
            continue
    inbs2 = inbs2 + 7 * max_ids
"""
# -- Filtering invalid values and replace
for k in range(np.shape(Zoomed_3)[0]):
    for i in range(np.shape(Zoomed_3)[1]):
        new = Zoomed_3[k, i]
        new = np.absolute(new)
        new = np.trunc(new)
        if (new == comp).all() == True:
            np.put(Zoomed_3, (inbs3 + (i * 7)), fill)
        else:
            continue
    inbs3 = inbs3 + 7 * max_ids

# -- Filtering invalid values and replace
for k in range(np.shape(Zoomed_4)[0]):
    for i in range(np.shape(Zoomed_4)[1]):
        new = Zoomed_4[k, i]
        new = np.absolute(new)
        new = np.trunc(new)
        if (new == comp).all() == True:
            np.put(Zoomed_4, (inbs4 + (i * 7)), fill)
        else:
            continue
    inbs4 = inbs4 + 7 * max_ids

# -- Filtering invalid values and replace
for k in range(np.shape(Zoomed_5)[0]):
    for i in range(np.shape(Zoomed_5)[1]):
        new = Zoomed_5[k, i]
        new = np.absolute(new)
        new = np.trunc(new)
        if (new == comp).all() == True:
            np.put(Zoomed_5, (inbs5 + (i * 7)), fill)
        else:
            continue
    inbs5 = inbs5 + 7 * max_ids

# -- Save as npy and csv
np.save('Preprocessed_1', Zoomed_1)
Zoomed_1 = Zoomed_1.tolist()
with open('Preprocessed_1.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile, delimiter=',')
    writer.writerows(Zoomed_1)
"""
# -- Save as npy and csv
np.save('Preprocessed_2', Zoomed_2)
Zoomed_2 = Zoomed_2.tolist()
with open('Preprocessed_2.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile, delimiter=',')
    writer.writerows(Zoomed_2)
"""
# -- Save as npy and csv
np.save('Preprocessed_3', Zoomed_3)
Zoomed_3 = Zoomed_3.tolist()
with open('Preprocessed_3.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile, delimiter=',')
    writer.writerows(Zoomed_3)

# -- Save as npy and csv
np.save('Preprocessed_4', Zoomed_4)
Zoomed_4 = Zoomed_4.tolist()
with open('Preprocessed_4.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile, delimiter=',')
    writer.writerows(Zoomed_4)

# -- Save as npy and csv
np.save('Preprocessed_5', Zoomed_5)
Zoomed_5 = Zoomed_5.tolist()
with open('Preprocessed_5.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile, delimiter=',')
    writer.writerows(Zoomed_5)
