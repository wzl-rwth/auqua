import numpy as np
import csv

"""
all None values are replaced by zeros
"""

# -- load raw data
data1 = np.load('data1_position.npy', allow_pickle=True)
data3 = np.load('data3_position.npy', allow_pickle=True)
data4 = np.load('data4_position.npy', allow_pickle=True)
data5 = np.load('data5_position.npy', allow_pickle=True)
data1 = data1.astype(float)
data3 = data3.astype(float)
data4 = data4.astype(float)
data5 = data5.astype(float)

# -- override Nan's to Zero's
new1 = np.nan_to_num(data1, copy=True, nan=0.0, posinf=None, neginf=None)
new3 = np.nan_to_num(data3, copy=True, nan=0.0, posinf=None, neginf=None)
new4 = np.nan_to_num(data4, copy=True, nan=0.0, posinf=None, neginf=None)
new5 = np.nan_to_num(data5, copy=True, nan=0.0, posinf=None, neginf=None)

# -- save as npy and csv
np.save('NaNtoNum_1.npy', new1)
np.save('NaNtoNum_3.npy', new3)
np.save('NaNtoNum_4.npy', new4)
np.save('NaNtoNum_5.npy', new5)

"""
new = new.tolist()
with open('NaNtoNum_5.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile, delimiter=',')
    writer.writerows(new)
"""
