import numpy as np
import quaternion
import math
import csv

# load Estimated States
data = np.load('M26_Estimated_States_imp_OC1000.npy', allow_pickle=True)

def quaternion_to_euler_angle_vectorized2(w, x, y, z):
    ysqr = y * y

    t0 = +2.0 * (w * x + y * z)
    t1 = +1.0 - 2.0 * (x * x + ysqr)
    X = np.degrees(np.arctan2(t0, t1))

    t2 = +2.0 * (w * y - z * x)

    t2 = np.clip(t2, a_min=-1.0, a_max=1.0)
    Y = np.degrees(np.arcsin(t2))

    t3 = +2.0 * (w * z + x * y)
    t4 = +1.0 - 2.0 * (ysqr + z * z)
    Z = np.degrees(np.arctan2(t3, t4))

    return X, Y, Z

# Swap Axis to (10, 2043, 7)
data = np.swapaxes(data, 0, 1)

# initial Array to fill, delete later
initial = np.empty((1, np.shape(data)[1], 6))
# loop through IDs
for k in range(np.shape(data)[0]):
    # init to append, delete later
    init = np.array([[0, 0, 0, 0, 0, 0]])
    # loop through time
    for i in range(np.shape(data)[1]):
        # stack coordinates and euler together
        coord = np.array([data[k, i, 0], data[k, i, 1], data[k, i, 2]])
        coord = coord * 10
        quat = np.array([data[k, i, 3], data[k, i, 4], data[k, i, 5], data[k, i, 6]])
        euler = quaternion_to_euler_angle_vectorized2(quat[0], quat[1], quat[2], quat[3])
        res = np.array([np.concatenate((coord, euler), axis=0)])
        init = np.append(init, res, axis=0)
    # delete first init and give 2 dim
    init = np.delete(init, 0, 0)
    init = np.array([init])
    # append initial
    initial = np.append(initial, init, axis=0)
# delete first initial entry
initial = np.delete(initial, 0, 0)

# Swap Axes for implementation (shape(poses))
initial = np.swapaxes(initial, 0,1)
initial = np.swapaxes(initial, 1,2)

# Save
initial = initial.tolist()
np.save('M26_Implementation_imp_OC1000.npy', initial)
with open('M26_Implementation_imp_OC1000.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile, delimiter=',')
    writer.writerows(initial)
