import numpy as np
import cv2
import cv2.aruco as aruco
import time

# --- define marker size in cm
marker_size = 2.5

# --- Get the camera calibration path
cameramat = [[1.03286902e+03, 0.0, 6.58907897e+02], [0.0, 1.03429138e+03, 3.45091281e+02], [0.0, 0.0, 1.0]]
cameradist = [[4.25944222e-01], [1.23255192e+01], [-4.37910090e-03], [3.03288210e-03], [1.92263934e+01], [3.09137573e-01], [1.20690128e+01], [1.90549537e+01]]
camera_matrix = np.asarray(cameramat)
camera_distortion = np.asarray(cameradist)

# --- Define the aruco dictionary
aruco_dict = aruco.getPredefinedDictionary(aruco.DICT_4X4_250)
parameters = aruco.DetectorParameters_create()
# --- Video capture
cap = cv2.VideoCapture(0)
cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)

# --- Define the codec and create VideoWriter object
fourcc = cv2.VideoWriter_fourcc(*'mp4v')
out = cv2.VideoWriter('Video_Cam1.mp4',fourcc, 3.0, (1280,720))

# Time loop for video
endtime = time.time() + 40

while time.time() < endtime:
    # -- read the camera frame
    ret, frame = cap.read()

    # -- Convert in gray scale
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # -- Find all the aruco markers in the image
    corners, ids, rejected = aruco.detectMarkers(image=gray, dictionary=aruco_dict, parameters=parameters,
                                                 cameraMatrix=camera_matrix, distCoeff=camera_distortion)
    # -- rotation and translation vectors
    rvec, tvec, _ = aruco.estimatePoseSingleMarkers(corners, marker_size, camera_matrix, camera_distortion)

    # -- Draw the detected marker and put a reference frame over it
    aruco.drawDetectedMarkers(frame, corners,ids)

    # -- draw the axes of markers coordinate system
    for i in ids:
        aruco.drawAxis(frame, camera_matrix, camera_distortion, rvec[i == ids], tvec[i == ids], 5)

    # -- write the flipped frame
    out.write(frame)

# Release everything if job is finished
cap.release()
out.release()
cv2.destroyAllWindows()