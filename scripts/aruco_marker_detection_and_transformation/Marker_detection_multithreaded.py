import numpy as np
import cv2
import cv2.aruco as aruco
import time
import quaternion
from Quaternion_calculation import quaternion_multiply, quaternion_negate_first
import pandas as pd
import threading

# ---Define Marker
marker_Length = 2.5

# --- Camera Calibration Data
cameramat = [[931.88954019, 0., 650.85325821], [0., 931.88954019, 361.40406118], [0., 0., 1.]]
cameradist = [[1.95538661e+00, 1.87783651e+01, -6.96730114e-04, 1.65345667e-03, -5.48381119e+01, 1.85057075e+00,
               1.86396271e+01, -5.31175474e+01]]
camera_matrix = np.asarray(cameramat)
camera_distortion = np.asarray(cameradist)

# --- ArUco Dictionary
aruco_dict = aruco.getPredefinedDictionary(aruco.DICT_4X4_250)
parameters = aruco.DetectorParameters_create()

# --- Initial Array
max_ids = 10
resultarray = np.empty((1, max_ids, 7), dtype=object)
assemblytime_arr = np.empty(1, dtype=object)
# --- Capture Camera
cap = cv2.VideoCapture(0)
# -- Set the camera size as the one it was calibrated with
cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)

# initialize thread
class DetectionThread(threading.Thread):
    def __init__(self, cap):
        threading.Thread.__init__(self)
        self.cap = cap
        self.stop = False

    def run(self):
        while(self.stop == False):
            # -- Read Camera Frame
            ret, frame = cap.read()
            # -- Convert in gray scale
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            # -- Detect Markers
            self.corners, self.ids, self.rejected = aruco.detectMarkers(image=gray, dictionary=aruco_dict,parameters=parameters,cameraMatrix=camera_matrix,
                                                                        distCoeff=camera_distortion)

    def Stop(self):
        self.stop = True

    def read(self):
        return self.corners, self.ids, self.rejected

# Start DetectionThread
thread = DetectionThread(0)
thread.start()
# Wait for Thread to start
time.sleep(1)

# --- Looptime definition (set Looptime > Calculationtime!)
Frequency = 1000
Looptime = 1/Frequency
assemblytime = 0

# -- Marker Detection
#if np.any(ids is not None):
endtime = time.time() + 30
starttime = time.time()
while time.time() < endtime:
    # get data from thread
    corners, ids, rejected = thread.read()
    # - Estimate pose of each marker and return the values
    rvec, tvec, _ = aruco.estimatePoseSingleMarkers(corners, marker_Length, camera_matrix, camera_distortion)

    # - 2D Array for data storage
    start = np.empty((max_ids, 7), dtype=object)

    # - rvec to rotation matrix
    Rot0 = np.array(cv2.Rodrigues(rvec[ids == 0])[0])

    # - Quaternion ID0 operations
    q0 = quaternion.from_rotation_vector(rvec[ids == 0].ravel())
    q0_arr = quaternion.as_float_array(q0)
    q0_neg = (-1 * q0_arr)

    for i in range(1, max_ids + 1):
        if i in ids:
            # tvec global, Marker ID0 is reference
            tvec_glob = -Rot0.T @ tvec[ids == 0].T + Rot0.T @ tvec[ids == i].T

            # rvecs to quaternion - check if rvec is valid
            rvec_to_quat = quaternion.from_rotation_vector(rvec[ids == i].ravel())
            q_arr = quaternion.as_float_array(rvec_to_quat)
            q_negate = quaternion_negate_first(q_arr)
            q_glob = quaternion_multiply(q_negate, q0_neg)
            q_result = quaternion_negate_first(q_glob)

            # tvec und rvec combined in 1D Array
            tvec_rvec = np.concatenate((tvec_glob.ravel(), q_result.ravel()))

            # Marker data included in 2D Array
            inbs = np.array([0, 1, 2, 3, 4, 5, 6])
            np.put(start, inbs + ((i - 1) * 7), [tvec_rvec])
    # - Array need third dimension
    start = np.array([start])

    # - Generating 3D Array
    resultarray = np.append(resultarray, start, axis=0)
    # - Time
    assemblytime = time.time() - starttime
    assemblytime_arr = np.append(assemblytime_arr, np.array([assemblytime]), axis=0)

# -- Stop thread
thread.Stop()
thread.join()

# -- Delete initial Nones
resultarray_new = np.delete(resultarray, 0, 0)
assemblytime_new = np.delete(assemblytime_arr, 0)
# -- Save Output Array's as npy-files
#np.save('data_position', resultarray_new)
#np.save('data_assemblytime', assemblytime_new)

# -- Snippet to write csv
import csv
resultarray_new = resultarray_new.tolist()
with open('data2_position.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile, delimiter=',')
    writer.writerows(resultarray_new)

pd.DataFrame(assemblytime_new).to_csv("data2_assemblytime.csv", header=False, index=False)