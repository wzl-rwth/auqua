import zmq

# set up client
context = zmq.Context()

# Socket to talk to server
socket1 = context.socket(zmq.REQ)

# - Connection Wlan / Lan Adress
socket1.connect('tcp://169.254.135.128:5551')

# send request
socket1.send(b"RaspberryPi1 ready!")

# Get the reply
message1 = socket1.recv()

if (message1 == b'Start'):
    # running detection programm
    import Detektion_RP1fix
