import time
import zmq

"""
first run via Putty on every Raspberry Client.py, then run Server.py on your Laptop
"""

# - initialize for every raspberry (client) one socket
context = zmq.Context()
socket1 = context.socket(zmq.REP)
#socket2 = context.socket(zmq.REP)
socket3 = context.socket(zmq.REP)
socket4 = context.socket(zmq.REP)
socket5 = context.socket(zmq.REP)

# - Connection Wlan / Lan Adress and port
# - IP adress of your laptop
#socket.bind("tcp://192.168.0.119:5555")
socket1.bind("tcp://169.254.135.128:5551")
#socket2.bind("tcp://169.254.135.128:5552")
socket3.bind("tcp://169.254.135.128:5553")
socket4.bind("tcp://169.254.135.128:5554")
socket5.bind("tcp://169.254.135.128:5555")

init = input("Type 'Check' to check connection: ")

while True:
    #  Wait for next request from client
    message1 = socket1.recv()
    print("Received request: %s" % message1)
    #message2 = socket2.recv()
    #print("Received request: %s" % message2)
    message3 = socket3.recv()
    print("Received request: %s" % message3)
    message4 = socket4.recv()
    print("Received request: %s" % message4)
    message5 = socket5.recv()
    print("Received request: %s" % message5)

    # starting
    init2 = input("Type 'Start' to start assembly: ")

    #  Send reply back to client
    socket1.send(b"Start")
    #socket2.send(b"Start")
    socket3.send(b"Start")
    socket4.send(b"Start")
    socket5.send(b"Start")

    # wait till detection starts
    time.sleep(6)

    # Start Assembly
    print("Assembly detection starts")

    # Timer for assembly - simultan to Raspberry
    start_time = 0
    while start_time < 61:
        print("Time is: ", start_time)
        time.sleep(1)
        start_time += 1
