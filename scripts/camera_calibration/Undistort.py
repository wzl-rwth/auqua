import cv2
import numpy as np

# insert camera matrix and distortion
matrix = [[1.02618121e+03, 0.00000000e+00, 6.23009725e+02], [0.00000000e+00, 1.02618121e+03, 3.89880222e+02], [0.00000000e+00, 0.00000000e+00, 1.00000000e+00]]
distortion = [0.11214186, -0.21062115, 0.00579172, -0.01476798, -2.33504778, -0.06837823, 0.1476806, -2.49137818]
mtx = np.asarray(matrix)
dist = np.asarray(distortion)

# read image
img = cv2.imread('Images/CharUcoBoard5.jpg')
h,  w = img.shape[:2]
newcameramtx, roi=cv2.getOptimalNewCameraMatrix(mtx,dist,(w,h),0,(w,h))

# undistort
dst = cv2.undistort(img, mtx, dist, None, newcameramtx)

# crop the image
x,y,w,h = roi
dst = dst[y:y+h, x:x+w]
# check result
cv2.imwrite('Camera_calibresult.png',dst)
# get new camera matrix
print(newcameramtx)