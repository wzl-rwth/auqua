Development and integration of an algorithm for markerbased object and process recognition with multi-camera system for manual assembly
-


Abstract
-
Increasing customer individuality and rising requirements in terms of quality, efficiency and sustainability are leading to greater product variance on the shop floor, which is also accompanied by greater product complexity. Companies are therefore only able to use automation technologies to a limited extent economically, so that assembly activities will continue to play a central role in future production. It can be assumed that the increasing assembly complexity and the demand for shorter assembly times will increase the employees' susceptibility to errors and thus also lead to rising error costs. To counteract this, human-centered assistance systems offer the possibility of reducing complexity with the help of augmented reality approaches.

The AuQuA project of the WZL at RWTH Aachen University addresses this issue by developing a multi-camera assistance system that independently recognizes and evaluates the process steps of manual assembly.

The goal of this Bachelor Thesis is to develop an algorithm that uses markers to localize components and hands and displays them in a global reference coordinate system. For this purpose, position estimates of the component and hand poses of several cameras are determined and subsequently a coordinate transformation and information fusion are performed. Both steps are statistically validated using appropriate methods. The algorithm will be integrated with the preliminary work done in the AuQuA project. The result of the integration shall be a visual MTM-compliant process sequence representation. Finally, an evaluation of the algorithm and an outlook on the feasibility of markerless object recognition will follow.

Translated with www.DeepL.com/Translator (free version)

Structure of this Repository
-

- scripts\
    |- Aruco_marker_detection_and_tranformation\
    ...|- Marker_detection.py.................................detection of aruco markers, global coordinates and orientation stored in npy.\
    ...|- Marker_detection_multithreaded.py...................multihreading approach, from 4 frames to 40 frames per second with raspberry pi 2\
    ...|- Quaternion_calculation.py...........................basic quaternion calculation\
    ...|- Videotake_of_detected_markers.py....................safe video of assembly, markers highlighted and marker coordinate system shown\


    |- Camera_calibration\
    ...|- Images\
    ...|- ChArUco_calibration.py..............................basic raspberry camera calibration with OpenCV and CharUco Board\
    ...|- Undistort.py........................................check the camera calibration result if valid\

    |- Fusion_unscented_kalman_filter\
    ...|- UnscentedKalmanFilter_fix_initial_value.py..........UKF from pykalman used, fusion of coordinates and orientations from five raspberry pi\
    ...|- UnscentedKalmanFilter_mean_initial_value.py.........see above, takes the mean value of first detected position data\
    ...|- UnscentedKalmanFilter_plotting.py...................plot each camera coordinates and the filter result\

    |- Implementation\
    ...|- Implementation.py...................................array calculation to implement the fusion result in project 20383149 on GitLab\

    |- Preprocessing\
    ...|- P1_nan_to_num.py....................................data preprocessing part1\
    ...|- P2_zoom_data_nparray.py.............................data preprocessing part2\
    ...|- P3_zero_to_none.py..................................data preprocessing part3\
    ...|- P4_imputation_window.py.............................data preprocessing part4\

    |- Server_client_system\
    ...|- Client.py...........................................client programm for every raspberry to start detection programm when server signal\
    ...|- Server.py...........................................control programm to start marker detection simultaniously on the raspberrys\

Requirements
-
- cv2
- numpy
- matplotlib
- pandas
- quaternion
- csv
- pylab
- pykalman
- scipy
- impyute
- zmq

